'''Faça um algoritmo para ler e armazenar em um vetor a temperatura média de todos os dias do ano. 
Calcular e escrever: 
a) Menor temperatura do ano 
b) Maior temperatura do ano 
c) Temperatura média anual 
d) O número de dias no ano em que a temperatura foi inferior a média anual
'''

import statistics

temperaturasDiarias = []
for i in range(3):
    entrada = float(input('Temperatura do dia {}:'.format(i+1)))
    temperaturasDiarias.append(entrada)
qtdDias=0
for i in range(3):
    if temperaturasDiarias[i] < statistics.mean(temperaturasDiarias):
        qtdDias = qtdDias + 1
print('\nMaior temperatura: {}\n'.format(max(temperaturasDiarias, key=float)))
print('Menor temperatura: {}\n'.format(min(temperaturasDiarias, key=float)))
print('Temperatura media: {}\n'.format(statistics.mean(temperaturasDiarias)))
print('Quantidade de dias abaixo da temp media: {}'.format(qtdDias))
