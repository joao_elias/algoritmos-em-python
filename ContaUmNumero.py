'''Faça um algoritmo para ler um vetor de 30 números. Após isto, ler mais um número qualquer, 
calcular e escrever quantas vezes esse número aparece no vetor. 
'''

tamanho = 30

dados = []
for i in range(tamanho):
    entrada = float(input('Valor {}:'.format(i+1)))
    dados.append(entrada)
verificador = float(input('Digite um valor para verificar se aparece no vetor e quantas vezes foi informado.\nValor: '))
vezes = 0
for i in range(tamanho):
    if dados[i] == verificador:
        vezes = vezes + 1
print('O numero {} aparece {} vezes'.format(verificador, vezes))
